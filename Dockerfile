FROM gitlab.scsuk.net:5005/scs-systems/ext_registry/centos:7.6.1810
COPY script /script
RUN yum -y install tftp-server tftp && \
    yum clean all
EXPOSE 69/udp
CMD /usr/sbin/in.tftpd -L -s /var/lib/tftpboot
