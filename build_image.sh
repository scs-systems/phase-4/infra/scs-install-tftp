#!/bin/bash
if [ "$SCSREL" = "" ] ; then
   echo "FAILURE: Must set SCSREL"
   exit 1
fi
IMAGE="gitlab.scsuk.net:5005/scs-systems/phase-4/scs-install-tftp:$SCSREL"
set -x
docker build --no-cache -t "$IMAGE" .
docker push "$IMAGE"
set +x
