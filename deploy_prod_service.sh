#!/bin/bash
if [ "$SCSREL" = "" ] ; then
   echo "FAILURE: Must set SCSREL"
   exit 1
fi

set -x # 
cat <<EOT | kubectl apply -n infra --force -f -
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata:
  name: scs-tftp
  namespace: infra
  labels:
    app: scs-tftp
spec:
  selector:
    matchLabels:
      app: scs-tftp
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: scs-tftp
    spec:
      containers:
      - image: gitlab.scsuk.net:5005/scs-systems/phase-4/scs-install-tftp:$SCSREL
        name: scs-tftp
        ports:
        - containerPort: 69
          name: tftp
          protocol: UDP
        volumeMounts:
        - mountPath: /var/lib/tftpboot
          name: tftp-data-persistent-storage
      volumes:
      - name: tftp-data-persistent-storage
        hostPath:
          path: /storage/static/scs-tftp/data
          type: Directory
      nodeSelector:
        kubernetes.io/hostname: jam4.local
      hostNetwork: true
EOT


echo
kubectl -n infra get deployment scs-tftp
echo
kubectl -n infra get pods -l app=scs-tftp -o wide
