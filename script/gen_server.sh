#!/bin/bash

# Pick a release
VALID_RELEASES=$(ls /var/lib/tftpboot/pxelinux/images)
echo '-----------------------------------------------------------'
echo "VALID_RELEASES.."
echo "$VALID_RELEASES"
echo '-----------------------------------------------------------'
RELEASE=${RELEASE:-"rhel-server-7.6-x86_64"}   # docker exec $CONTAINER ls /var/lib/tftpboot/pxelinux/images

# Provide client details
MAC=${MAC:-"01-b8-ae-ed-eb-7f-a9"}
CLIENT_IP=${CLIENT_IP:-"192.168.1.211"}
GATEWAY=${GATEWAY:-"192.168.1.254"}
NETMASK=${NETMASK:-"255.255.255.0"}
CLIENT_HOSTNAME=${CLIENT_HOSTNAME:-"jam2.local"}
DNS_SERVER_1=${DNS_SERVER_1:-"192.168.1.210"}
DNS_SERVER_2=${DNS_SERVER_2:-"192.168.1.255"}
NTP_SERVER=""
KICKSTART_SERVER=${KICKSTART_SERVER:-"http://www.scs-install-kickstart.scsuk.net"}
KICKSTART_FILE="$KICKSTART_SERVER/ks.cfg".$RELEASE

# Start building the pxelinux.cfg/server file
PXE_CONFFILE=/var/lib/tftpboot/pxelinux/pxelinux.cfg/"$MAC"
cat <<EOT >"$PXE_CONFFILE"
UI vesamenu.c32
DEFAULT $RELEASE
prompt 1
MENU RESOLUTION 1024 768
menu title  Install Menu : (CLIENT_HOSTNAME($CLIENT_HOSTNAME),RELEASE($RELEASE)
ONTIMEOUT $RELEASE
SAY Installing RELEASE($RELEASE),CLIENT_HOSTNAME($CLIENT_HOSTNAME)
TIMEOUT 100
LABEL $RELEASE
  kernel images/$RELEASE/vmlinuz
  append initrd=images/$RELEASE/initrd.img ip=$CLIENT_IP::$GATEWAY:$NETMASK:$CLIENT_HOSTNAME::none:$DNS_SERVER_1:$DNS_SERVER_2:$NTP_SERVER inst.ks=$KICKSTART_FILE
EOT

ls -l "$PXE_CONFFILE"
echo '-----------------------------------------------------------'
cat "$PXE_CONFFILE"
echo '-----------------------------------------------------------'

