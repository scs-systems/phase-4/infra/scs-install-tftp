#!/bin/bash -eu

RELEASE=CentOS-7.6-x86_64-Minimal
HTTP_REPO_DISTRO=${HTTP_REPO_DISTRO:-http://www.scs-install-distro.local}
HTTP_REPO_KS=${HTTP_REPO_KS:-http://www.scs-install-kickstart.local}

echo '######################################################################################'
echo '# PXE boot files'
PXEBOOTDIR=/var/lib/tftpboot/pxelinux
mkdir -p "$PXEBOOTDIR"

if [ "$RELEASE" = "CentOS-7.6-x86_64-Minimal" ] ; then
   # syslinux is not on the install CD - have to download the rpm and get pxelinux.0 and vesamenu.c32 from the RPM
   echo '# Install syslinux'
   yum -y install syslinux
   echo "# Copy pxelinux.0 and vesamenu.c32 to $PXEBOOTDIR"
   cp -v /usr/share/syslinux/pxelinux.0 "$PXEBOOTDIR"
   cp -v /usr/share/syslinux/vesamenu.c32 "$PXEBOOTDIR"
fi

echo '--------------------------------------------------'
ls -l "$PXEBOOTDIR"/pxelinux.0 "$PXEBOOTDIR"/vesamenu.c32

echo '######################################################################################'
echo '# Next the OS boot files '
RELEASE_DIR="/var/lib/tftpboot/pxelinux/images/$RELEASE"
mkdir -p "$RELEASE_DIR"

echo "# Pull $HTTP_REPO_DISTRO/$RELEASE/images/pxeboot/initrd.img to $RELEASE_DIR/initrd.img"
curl "$HTTP_REPO_DISTRO"/"$RELEASE"/images/pxeboot/initrd.img -o "$RELEASE_DIR"/initrd.img
echo "# Pull $HTTP_REPO_DISTRO/$RELEASE/images/pxeboot/vmlinuz to $RELEASE_DIR/vmlinuz"
curl "$HTTP_REPO_DISTRO"/"$RELEASE"/images/pxeboot/vmlinuz -o "$RELEASE_DIR"/vmlinuz

echo '--------------------------------------------------'
ls -l "$RELEASE_DIR"/initrd.img "$RELEASE_DIR"/vmlinuz

echo '######################################################################################'
echo '# Next build the pxelinux.cfg/default file'
PXE_CONFDIR=/var/lib/tftpboot/pxelinux/pxelinux.cfg
mkdir -p "$PXE_CONFDIR"

function gen_pxelinux_conf_linux
{
   RELEASE=$1
   MNUM=$2
   cat <<EOT
label ${RELEASE}_linux
  menu label ^$MNUM) $RELEASE Install system
  menu default
  kernel images/$RELEASE/vmlinuz
  append initrd=images/$RELEASE/initrd.img ip=dhcp inst.repo=$HTTP_REPO_DISTRO/$RELEASE/ inst.ks=$HTTP_REPO_KS/ks.cfg.$RELEASE

EOT
}

function gen_pxelinux_conf_vesa
{
   RELEASE=$1
   MNUM=$2
  cat <<EOT
label ${RELEASE}_vesa
  menu label ^$MNUM) $RELEASE Install system with basic video driver
  kernel images/$RELEASE/vmlinuz
  append initrd=images/$RELEASE/initrd.img ip=dhcp inst.xdriver=vesa nomodeset inst.repo=$HTTP_REPO_DISTRO/$RELEASE/

EOT
}

function gen_pxelinux_conf_rescue
{
   RELEASE=$1
   MNUM=$2
  cat <<EOT
label ${RELEASE}_rescue
  menu label ^$MNUM) $RELEASE Rescue installed system
  kernel images/$RELEASE/vmlinuz
  append initrd=images/$RELEASE/initrd.img rescue

EOT
}

PXE_CONFFILE="$PXE_CONFDIR"/default
MNUM=1

cat >$PXE_CONFFILE <<EOT
# Menu UI
UI vesamenu.c32
# Default boot option to use
DEFAULT local
# Prompt user for selection
prompt 1
# how long to wait until booting automatically

MENU RESOLUTION 1024 768
menu title  Install Menu : (default = "Boot from local drive")
timeout 100
ONTIMEOUT local

label local
  menu label ^$MNUM) Boot from local drive
  localboot 0xffff

EOT

(( MNUM = MNUM + 1 ))
gen_pxelinux_conf_linux $RELEASE $MNUM >>$PXE_CONFFILE
(( MNUM = MNUM + 1 ))
gen_pxelinux_conf_vesa $RELEASE $MNUM >>$PXE_CONFFILE
(( MNUM = MNUM + 1 ))
gen_pxelinux_conf_rescue $RELEASE $MNUM >>$PXE_CONFFILE

echo '--------------------------------------------------'
ls -l "$PXE_CONFFILE"
echo '######################################################################################'
